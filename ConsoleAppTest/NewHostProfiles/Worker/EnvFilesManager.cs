﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HostFileManager
{
    public class EnvFilesManager
    {
        internal void UpdateEnvFiles(List<DeployEnvironment> hostList)
        {
            for (int i = 0; i < hostList.Count; i++)
            {
                DeployEnvironment item = hostList[i];
                WriteFile(item);
            }
        }
        private void WriteFile(DeployEnvironment item)
        {
            string outputDirectory = GetEnvFilesPath();
            Directory.CreateDirectory(outputDirectory);

            string fileName = Path.Combine(outputDirectory, $"{item.Name}.txt");
            try
            {
                bool shouldWriteFile = ShouldWriteContent(fileName, item.Content);

                if (shouldWriteFile)
                    File.WriteAllText(fileName, item.Content);
            }
            catch (Exception ex)
            {
                App.Tracer.Error(ex.ToString());
            }
        }

        private static bool ShouldWriteContent(string fileName, string content)
        {
            bool resp = true;

            if (File.Exists(fileName))
            {
                string fileContent = File.ReadAllText(fileName);
                if (fileContent.Equals(content))
                    resp = false;
            }

            return resp;
        }

        public string GetEnvFilesPath()
        {
            return $"{Environment.CurrentDirectory}\\Environments";
        }

    }
}
