﻿using System;
using System.IO;
using System.Text;
using System.Windows;

namespace HostFileManager
{
    public class HostFileBuilder
    {
        public bool SaveHost(string content)
        {
            bool resp = false;

            Exception occurredEx = null;
            try
            {
                DirectoryInfo di = new DirectoryInfo(Environment.GetFolderPath(Environment.SpecialFolder.System));
                string path = Path.Combine(di.FullName, "drivers", "etc", "hosts");
                File.WriteAllText(path, content);
                resp = true;
            }
            catch (Exception ex)
            {
                occurredEx = ex;
            }

            if (!resp)
            {
                string message;
                string baseMessage = "Impossibile salvare il file host.\nAssicurarsi di aver avviato il programma come Amministratore";
                if (occurredEx is null)
                    message = baseMessage;
                else
                {
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine(baseMessage);
                    sb.AppendLine();
                    sb.AppendLine("Eccezione:");
                    sb.AppendLine(occurredEx.Message);
                    sb.AppendLine();
                    sb.AppendLine("StackTrace:");
                    sb.AppendLine(occurredEx.StackTrace);
                    message = sb.ToString();
                }
                MessageBox.Show(message);
            }

            return resp;

        }


        public string GetCurrentHostProfileText()
        {
            DirectoryInfo di = new DirectoryInfo(Environment.GetFolderPath(Environment.SpecialFolder.System));
            string path = Path.Combine(di.FullName, "drivers", "etc", "hosts");
            return File.ReadAllText(path);
        }
    }
}