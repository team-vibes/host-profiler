﻿using HostFileManager.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HostFileManager
{
    public class OptionManager
    {
        private Options Options { get; set; }
        public void LoadOptions()
        {
            string fullFileName = GetOptionsFile();
            if (File.Exists(fullFileName))
                Options = SerializationHelper.Deserialize<Options>(fullFileName);
            else
                Options = new Options();

        }

        public void SaveOptions(Options options)
        {
            SerializationHelper.Serialize(options, GetOptionsFile());
        }

        public Options GetOptions()
        {
            return Options;
        }
        public string GetOptionsFile()
        {
            return Path.Combine(Environment.CurrentDirectory, "configs", "Options.xml");
        }
    }
}
