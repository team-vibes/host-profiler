﻿using HostFileManager.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HostFileManager
{
    public class ViewModelManager
    {
        public const string AllCategoryText = "Tutti";
        internal void UpdateViewModel(EnvironmentsContentDownloader myWorker, MainModel mainModel, Options options)
        {
            UpdateEnvironment(mainModel,myWorker.HostList, options);
            UpdateOverrideEnv(mainModel,myWorker.OverrideFile.Content);
            LoadStaticDevs(mainModel);
            UpdateCurrentHostProfileText(mainModel);
            UpdateNetworkInfoText(mainModel);
            FillCategorySelector(mainModel,myWorker.HostList,options);
        }

        private void UpdateEnvironment(MainModel mainModel,List<DeployEnvironment> hostList, Options options)
        {
            mainModel.Environments.Clear();
            foreach (var env in hostList)
            {
                DeployEnvironmentModel model = new DeployEnvironmentModel();
                model.Category = env.Category;
                model.Name = env.Name;
                model.Uri = env.URI;
                model.Content = env.Content;
                //FillCategorySelector(model);     
                mainModel.Environments.Add(model);

                if (string.Equals(model.Name, options.LastFlushedEnvironment, StringComparison.OrdinalIgnoreCase))
                    model.IsCurrent = true;
            }
        }

        public void UpdateOverrideEnv(MainModel mainModel, string content)
        {
            mainModel.OverrideEnv.Content = content;
        }

        private void LoadStaticDevs(MainModel mainModel)
        {
            string staticDevsPath = Path.Combine(Environment.CurrentDirectory, "StaticDevs");
            DirectoryInfo dir = new DirectoryInfo(staticDevsPath);
            if (!dir.Exists)
                return;

            FileInfo[] files = dir.GetFiles("*.txt");
            foreach (FileInfo file in files)
            {
                DeployEnvironmentModel model = new DeployEnvironmentModel();
                model.Name = Path.GetFileNameWithoutExtension(file.FullName);
                model.Content = File.ReadAllText(file.FullName);
                model.Category = "Static";
                model.Uri = "";

                mainModel.Environments.Insert(0, model);
            }

        }

        public void UpdateCurrentHostProfileText(MainModel mainModel)
        {
            HostFileBuilder hostFileBuilder = new HostFileBuilder();
            mainModel.CurrentHostProfileText = hostFileBuilder.GetCurrentHostProfileText();
        }

        public void UpdateNetworkInfoText(MainModel mainModel)
        {
            mainModel.NetworkInfoText = GetNetworkInfo();
        }
        private string GetNetworkInfo()
        {
            DirectoryInfo di = new DirectoryInfo(Environment.GetFolderPath(Environment.SpecialFolder.System));
            string str = string.Empty;
            using (System.Diagnostics.Process p = new System.Diagnostics.Process())
            {
                p.StartInfo = new System.Diagnostics.ProcessStartInfo();
                p.StartInfo.FileName = Path.Combine(di.FullName, "ipconfig.exe");
                //p.StartInfo.Arguments = arges;
                p.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                p.StartInfo.RedirectStandardOutput = true;
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.CreateNoWindow = true;
                p.Start();
                StreamReader reader = p.StandardOutput;

                str = reader.ReadToEnd();

            }
            return str;
        }


        private void FillCategorySelector(MainModel mainModel, List<DeployEnvironment> hostList, Options options)
        {
            mainModel.CategorySelector.Clear();
            mainModel.SelectedCategory = "";
            mainModel.CategorySelector.Add(AllCategoryText);


            foreach (var item in hostList.Select(a => a.Category).Distinct())
            {
                mainModel.CategorySelector.Add(item);
            }

            if (mainModel.CategorySelector.Contains(options.LastSelectedCategory))
                mainModel.SelectedCategory = options.LastSelectedCategory;
            else
                mainModel.SelectedCategory = AllCategoryText;
        }

        internal void UpdateOptions(MainModel mainModel, Options options)
        {
            mainModel.SelectedCategory = options.LastSelectedCategory;
        }
    }
}
