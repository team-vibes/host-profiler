﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HostFileManager
{
    public static class DeployEnvironmentsLoader
    {

        public static List<DeployEnvironment> Load(string fullFileName)
        {
            var deserialized = SerializationHelper.Deserialize<DeployEnvironmentCollection>(fullFileName);
            return deserialized.Env.ToList();
        }
    }
}
