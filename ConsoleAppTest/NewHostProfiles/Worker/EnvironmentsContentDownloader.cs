﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace HostFileManager
{
    public class EnvironmentsContentDownloader
    {

        // HttpClient is intended to be instantiated once per application, rather than per-use. See Remarks.
        private readonly HttpClient Client = new HttpClient();
        public List<DeployEnvironment> HostList { get; private set; } 
        public EnvironmentOverrides OverrideFile { get; private set; }

        public CancellationToken canc = new CancellationToken();
        public static CancellationTokenSource cancSource = new CancellationTokenSource();

        private const string EnvironmentKeyPrefix = "Env_";


        public void Initialize()
        {
            if (!ReadEnvironments())
                return;
            ReadEnvironmentOverrides();
        }


        public void ReadEnvironmentOverrides()
        {
            try
            {
                OverrideFile = new EnvironmentOverrides();
                OverrideFile.Content = GetOverrideFileContent();
            }
            catch (NotSupportedException e)
            {
                App.Tracer.Error(e);
                App.Tracer.Error("Unable to calculate folder size: {0}", e);
            }

        }

        public static string GetOverrideFileContent()
        {
            string resp;

            string overrideFileFullName = GetOverrideFileFullName();
            if (File.Exists(overrideFileFullName))
                resp = File.ReadAllText(overrideFileFullName);
            else
                resp = string.Empty;

            return resp;
        }

        private bool ReadEnvironments()
        {
            string fullFileName = GetEnvFileFullName();
            if (string.IsNullOrEmpty(fullFileName))
                return false;
            HostList = DeployEnvironmentsLoader.Load(fullFileName);
            return true;
        }

        private string GetEnvFileFullName()
        {
            string environmentPath = Path.Combine(Environment.CurrentDirectory,"configs", "Environments.xml");
            string defaultEnvironmentPath = Path.Combine(Environment.CurrentDirectory, "DefaultEnvs", "DefaultEnvironments.xml");
            if (File.Exists(environmentPath))
                return environmentPath;
            if (!File.Exists(environmentPath) && File.Exists(defaultEnvironmentPath))
            {
                File.Copy(defaultEnvironmentPath, environmentPath);
            }
            else
                environmentPath = string.Empty;
            return environmentPath;
        }


        public async Task GetContentAsync(CancellationToken canc)
        {

            for (int j = 0; j < HostList.Count; j++)
            {
                DeployEnvironment item = HostList[j];
                string uriDev = item.URI;

                try
                {
                    HttpResponseMessage response = await Client.GetAsync(uriDev, canc);
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();
                    item.Content = responseBody;
                    //string ResponseBody = await Client.GetStringAsync("http://www.blognew.aruba.it/");

                }
                catch (HttpRequestException e)
                {
                    App.Tracer.Error(e);
                }

            }
        }

        public static string GetOverrideFileFullName()
        {
            return Path.Combine(Environment.CurrentDirectory, "configs", "Overrides.txt");
        }


    }

}
