﻿using HostFileManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HostFileManager
{
    /// <summary>
    /// Interaction logic for SearchWindow.xaml
    /// </summary>
    public partial class SearchWindow : Window
    {
        public SearchWindow()
        {
            InitializeComponent();
            LoadLastSearch();
            FocusAndHighlightTextBox();

        }

        private void FocusAndHighlightTextBox()
        {
            SearchBox.Focus();
            SearchBox.Select(0, SearchBox.Text.Length);
        }

        public SearchModel Model => (SearchModel)DataContext;

        public void SearchEnvironmentButton_Click(object sender, RoutedEventArgs e)
        {
            Model.ExecuteDoSearchCommand();

            this.DialogResult = false;
        }
        
        public void SearchCancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }



        private void LoadLastSearch()
        {
            OptionManager optionManager = new OptionManager();
            optionManager.LoadOptions();
            Options options = optionManager.GetOptions();
            Model.StringToSearch = options.LastStringSearched;
            switch (options.Context)
            {
                case SearchContext.CurrentEnvironment:
                    Model.CurrentEnvironment = true;
                    break;
                case SearchContext.CurrentCategory:
                    Model.CurrentCategory = true;
                    break;
                case SearchContext.All:
                    Model.AllEnvironment = true;
                    break;
                default:
                    break;
            }

        }
    }
}
