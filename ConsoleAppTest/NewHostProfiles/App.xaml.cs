﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using log4net;

namespace HostFileManager
{
    /// <summary>
    /// Logica di interazione per App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static readonly ILog Tracer = LogManager.GetLogger(typeof(App));
        
               
        private void ApplicationStartup(object sender, StartupEventArgs e)
        {
            Tracer.Info($"Avvio procedura Host Profiles");

            MainWindow mainWindow = new MainWindow();                        
            //SearchWindow mainWindow = new SearchWindow();
            mainWindow.Show();
           
        }        

    }
}
