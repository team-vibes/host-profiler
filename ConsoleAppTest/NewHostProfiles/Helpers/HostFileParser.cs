﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace HostFileManager
{
    public class HostFileParser
    {
        private const string CommentPattern = @"^\s*#";
        private const string EntryPattern = @"^\s*(?<ipAddress>((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))\s+(?<domainName>[^\s]+)(\s*$|\s+.+$)";
        public HostFileParser(string fileContent)
        {
            FileContent = fileContent;
        }

        public string FileContent { get; }
        private List<HostFileRow> Response;

        public List<HostFileRow> Parse()
        {
            Response = new List<HostFileRow>();
            DoParse();
            return Response;
        }

        private void DoParse()
        {
            if (string.IsNullOrWhiteSpace(FileContent))
                return;

            string[] lines = Regex.Split(FileContent, "\r\n|\r|\n");
            ParseLines(lines);
        }

        private void ParseLines(string[] lines)
        {
            foreach (string line in lines)
            {
                ParseLine(line);
            }
        }

        private void ParseLine(string line)
        {
            HostFileRow row = new HostFileRow(line);
            Response.Add(row);

            if (string.IsNullOrWhiteSpace(line))
                return;

            if (StartWithEscapedChar(line))
                return;

            FillEntryValues(row);
        }

        private void FillEntryValues(HostFileRow row)
        {
            Regex re = new Regex(EntryPattern);
            Match ma = re.Match(row.Content);
            if (!ma.Success)
                return;

            string ipAddress = ma.Groups["ipAddress"].Value;
            string domainName = ma.Groups["domainName"].Value;
            row.SetEntryValues(ipAddress, domainName);
        }

        private bool StartWithEscapedChar(string line)
        {
            Regex re = new Regex(CommentPattern);
            Match ma = re.Match(line);
            return ma.Success;
        }
    }
}
