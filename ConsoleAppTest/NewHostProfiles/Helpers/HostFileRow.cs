﻿namespace HostFileManager
{
    public class HostFileRow
    {
        public HostFileRow(string content)
        {
            Content = content.Trim();
        }

        public string Content { get; }
        public bool IsEntry { get; private set; }
        public string IpAddress { get; private set; }
        public string DomainName { get; private set; }

        public void SetEntryValues(string ipAddress, string domainName)
        {
            IsEntry = true;
            IpAddress = ipAddress;
            DomainName = domainName;
        }
    }
}
