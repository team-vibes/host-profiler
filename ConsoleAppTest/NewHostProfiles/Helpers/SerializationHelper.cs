﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace HostFileManager
{
    public static class SerializationHelper
    {
        public static void Serialize(object obj,  string fullFileName)
        {
            if (obj is null)
                throw new ArgumentNullException(nameof(obj));

            if (File.Exists(fullFileName))
                File.Delete(fullFileName);

            using (var writer = new System.IO.StreamWriter(fullFileName))
            {
                var serializer = new XmlSerializer(obj.GetType());
                serializer.Serialize(writer, obj);
                writer.Flush();
            }
        }

        public static T Deserialize<T>(string fileName)
        {
            using (var stream = System.IO.File.OpenRead(fileName))
            {
                var serializer = new XmlSerializer(typeof(T));
                return (T)serializer.Deserialize(stream);
            }
        }
    }
}
