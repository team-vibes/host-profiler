﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace HostFileManager.Models
{
    public class SearchModel : ViewModelBase
    {

        private ICommand m_DoSearchCommand;
        public ICommand DoSearchCommand
        {
            get => m_DoSearchCommand ?? (m_DoSearchCommand = new CommandHandler(() => ExecuteDoSearchCommand(), () => CanExecuteFindCommand));
        }

        public bool CanExecuteFindCommand => true;


        private string m_StringToSearch;
        public string StringToSearch
        {
            get => m_StringToSearch;
            set => SetProperty(ref m_StringToSearch,value);
        }

        private bool m_CurrentEnvironment;
        public bool CurrentEnvironment
        {
            get => m_CurrentEnvironment;
            set => SetProperty(ref m_CurrentEnvironment, value);
        }

        private bool m_CurrentCategory;
        public bool CurrentCategory
        {
            get => m_CurrentCategory;
            set => SetProperty(ref m_CurrentCategory, value);
        }

        private bool m_AllEnvironment;
        public bool AllEnvironment
        {
            get => m_AllEnvironment;
            set => SetProperty(ref m_AllEnvironment, value);
        }

        public Action<SearchRequest> DoSearch { get; internal set; }

        public void ExecuteDoSearchCommand()
        {
            SearchRequest request = CreateRequest();
            DoSearch.Invoke(request);
        }

        private SearchRequest CreateRequest()
        {
            SearchRequest request = new SearchRequest();

            request.StringToSearch = StringToSearch;
            if (CurrentEnvironment)
                request.Context = SearchContext.CurrentEnvironment;
            if (CurrentCategory)
                request.Context = SearchContext.CurrentCategory;
            if (AllEnvironment)
                request.Context = SearchContext.All;
            return request;
        }

        public void CancelSearchCommand()
        {
            throw new NotImplementedException();
        }
    }
}
