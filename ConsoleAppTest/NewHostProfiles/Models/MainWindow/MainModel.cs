﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace HostFileManager.Models
{
    public class MainModel : ViewModelBase
    {
        public ObservableCollection<DeployEnvironmentModel> Environments { get; set; } = new ObservableCollection<DeployEnvironmentModel>();
        public EnviromentOverridesModel OverrideEnv { get; set; } = new EnviromentOverridesModel();
        public ObservableCollection<string> CategorySelector { get; set; } = new ObservableCollection<string>();

        private bool m_UseOverrides;
        public bool UseOverrides
        {
            get => m_UseOverrides;
            set => SetProperty(ref m_UseOverrides, value);
        }

        private string m_SelectedCategory;
        public string SelectedCategory
        {
            get => m_SelectedCategory;
            set => SetProperty(ref m_SelectedCategory, value);
        }

        private string m_CurrentHostProfileText;
        public string CurrentHostProfileText
        {
            get => m_CurrentHostProfileText;
            set => SetProperty(ref m_CurrentHostProfileText, value);

        }

        private string m_NetworkInfoText;
        public string NetworkInfoText
        {
            get => m_NetworkInfoText;
            set => SetProperty(ref m_NetworkInfoText, value);
        }

        private bool m_IsWorking;
        public bool IsWorking
        {
            get => m_IsWorking;
            set
            {
                m_IsWorking = value;
                OnPropertyChanged(nameof(m_IsWorking));
                if (value)
                {
                    StartButtonVisibility = Visibility.Collapsed;
                    CancelButtonVisibility = Visibility.Visible;
                    EnvFolderButtonVisibility = Visibility.Visible;
                }
                else
                {
                    StartButtonVisibility = Visibility.Visible;
                    CancelButtonVisibility = Visibility.Collapsed;
                    EnvFolderButtonVisibility = Visibility.Visible;
                }
            }

        }

        private Visibility m_StartButtonVisibility = Visibility.Visible;
        public Visibility StartButtonVisibility
        {
            get => m_StartButtonVisibility;
            set => SetProperty(ref m_StartButtonVisibility, value);
        }

        private Visibility m_CancelButtonVisibility = Visibility.Collapsed;
        public Visibility CancelButtonVisibility
        {
            get => m_CancelButtonVisibility;
            set => SetProperty(ref m_CancelButtonVisibility, value);
        }


        private Visibility m_EnvFolderButtonVisibility = Visibility.Visible;
        public Visibility EnvFolderButtonVisibility
        {
            get => m_EnvFolderButtonVisibility;
            set => SetProperty(ref m_EnvFolderButtonVisibility, value);
        }

        private ICommand m_FindCommand;
        public ICommand FindCommand
        {
            get => m_FindCommand ?? (m_FindCommand = new CommandHandler(() => ExecuteFindCommand(), () => CanExecuteFindCommand));
        }

        public bool CanExecuteFindCommand => StartButtonVisibility == Visibility.Visible;
        
        public void ExecuteFindCommand()
        {
            SearchWindow searchWindow = new SearchWindow();
            searchWindow.Model.DoSearch = DoSearch;
            searchWindow.ShowDialog();
        }

        private void DoSearch(SearchRequest request)
        {
            LoadOptions(request);

        }

        private static void LoadOptions(SearchRequest request)
        {
            OptionManager optionManager = new OptionManager();
            optionManager.LoadOptions();
            Options options = optionManager.GetOptions();
            options.Context = request.Context;
            options.LastStringSearched = request.StringToSearch;
            optionManager.SaveOptions(options);
        }
    }

    public enum SearchContext
    {
        CurrentEnvironment,
        CurrentCategory,
        All
    }
    public class SearchRequest
    {
        public string StringToSearch { get; set; }
        public SearchContext Context { get; set; }
    }

}
