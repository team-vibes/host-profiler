﻿using System.Windows;

namespace HostFileManager.Models
{
    public class DeployEnvironmentModel : ViewModelBase
    {

        private Visibility m_Visible = Visibility.Visible;
        public Visibility Visible
        {
            get => m_Visible;
            set => SetProperty(ref m_Visible, value);
        }

        private bool m_IsCurrent;
        public bool IsCurrent
        {
            get => m_IsCurrent;
            set
            {
                SetProperty(ref m_IsCurrent, value);
                IsImageVisible = IsCurrent ? Visibility.Visible : Visibility.Hidden;
            }
        }

        private Visibility m_IsImageVisible = Visibility.Hidden;
        public Visibility IsImageVisible
        {
            get => m_IsImageVisible;
            set => SetProperty(ref m_IsImageVisible, value);
        }

        private string m_Name;
        public string Name
        {
            get => m_Name;
            set => SetProperty(ref m_Name, value);
        }

        private string m_Content;
        public string Content
        {
            get => m_Content;
            set => SetProperty(ref m_Content, value);
        }

        private string m_Category;
        public string Category
        {
            get => m_Category;
            set => SetProperty(ref m_Category, value);
        }

        private string m_Uri;
        public string Uri
        {
            get => m_Uri;
            set => SetProperty(ref m_Uri, value);
        }

    }
}