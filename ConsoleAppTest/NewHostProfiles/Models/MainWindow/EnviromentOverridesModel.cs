﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HostFileManager.Models
{
    public class EnviromentOverridesModel : ViewModelBase
    {        
        private string m_Content;

        public string Content
        {
            get => m_Content;
            set => SetProperty(ref m_Content, value);
        }

    }
}
