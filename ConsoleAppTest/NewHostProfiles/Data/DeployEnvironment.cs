﻿using System;
using System.Xml.Serialization;

namespace HostFileManager
{
    [Serializable()]
    [XmlType(AnonymousType = true)]
    public class DeployEnvironment
    {
        [XmlAttribute("category")]
        public string Category { get; set; } //Dev, Test o Unici
        [XmlAttribute("name")]
        public string Name { get; set; }
        [XmlAttribute("uri")]
        public string URI { get; set; }
        [XmlIgnore()]
        public string Content { get; set; }

    }
}
