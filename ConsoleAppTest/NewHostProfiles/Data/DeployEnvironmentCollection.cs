﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace HostFileManager
{
    [Serializable()]
    [XmlType(AnonymousType = true)]
    [XmlRoot("Environments", Namespace = "", IsNullable = false)]
    public class DeployEnvironmentCollection
    {
        [XmlElement("Env")]
        public DeployEnvironment[] Env { get; set; }
    }
}
