﻿using HostFileManager.Models;
using System;
using System.Xml.Serialization;

namespace HostFileManager
{
    [Serializable]
    public class Options
    {
        [XmlAttribute]
        public string LastFlushedEnvironment { get; set; }
        [XmlAttribute]
        public string LastSelectedCategory { get; set; }
        [XmlAttribute]
        public bool UseOverridesSaved { get; set; }
        [XmlAttribute]
        public string LastStringSearched { get; set; }
        [XmlAttribute]
        public SearchContext Context { get; set; }
    }
}
