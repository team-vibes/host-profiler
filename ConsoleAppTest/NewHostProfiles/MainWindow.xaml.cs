﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using log4net;
using HostFileManager.Models;
using MessageBox = System.Windows.Forms.MessageBox;

namespace HostFileManager
{
    /// <summary>
    /// Logica di interazione per MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public const string OverridenPrefix = "#Overridden: ";
        public const string AllCategoryText = "Tutti";


        public OptionManager OptionManager;
        public MainModel MainModel => (MainModel)DataContext;
        private ViewModelManager ViewModelManager;
        public MainWindow()
        {
            ViewModelManager = new ViewModelManager();
            InitializeComponent();
            EnvironmentsListView.Items.Filter = UserFilter;
            OptionManager = new OptionManager();
            OptionManager.LoadOptions();
            UpdateCurrentHostProfileText();
            ViewModelManager.UpdateNetworkInfoText(MainModel);
            ViewModelManager.UpdateOptions(MainModel,OptionManager.GetOptions());
        }

        private bool UserFilter(object item)
        {
            bool resp;

            DeployEnvironmentModel envModel = (DeployEnvironmentModel)item;
            if (string.Equals(envModel.Category, "Static"))
                resp = true;
            else if (string.Equals(envModel.Category, MainModel.SelectedCategory, StringComparison.OrdinalIgnoreCase))
                resp = true;
            else if (string.Equals(AllCategoryText, MainModel.SelectedCategory, StringComparison.OrdinalIgnoreCase))
                resp = true;
            else
                resp = false;

            return resp;
        }

        protected void WindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (MessageBox.Show("Sei sicuro di voler uscire ? ", "Conferma chiusura", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No)
            {
                e.Cancel = true;
            }
        }
        
        private void EventCancelClick(object sender, RoutedEventArgs e)
        {
            EnvironmentsContentDownloader.cancSource.Cancel();
        }

        private void EnvFolderButtonClick(object sender, RoutedEventArgs e)
        {
            EnvFilesManager envFilesBuilder = new EnvFilesManager();
            Process.Start(envFilesBuilder.GetEnvFilesPath());
        }

        private void InfoButtonClick(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Sviluppato dal Team Vibes di SCAI-Axot\n - Giada Zuin\n - Massimo Lombardini\n - Abdoullaj Fall\n - Mauro Arduini", "Host File Manager Info");
        }
        
        

        private async void WorkerStartButtonClick(object sender, RoutedEventArgs e)
        {
            Dispatcher.Invoke(() =>
             {
                 MainModel.IsWorking = true;
             });
            await UpdateEnvironments();
            if (MainModel.Environments.Count > 0)
                EnvironmentsListView.SelectedIndex = 0;
            Dispatcher.Invoke(() =>
            {
                MainModel.IsWorking = false;
            });

        }

        private async Task UpdateEnvironments()
        {
            EnvironmentsContentDownloader envDownloader;
            envDownloader = new EnvironmentsContentDownloader();
            envDownloader.Initialize();
            await envDownloader.GetContentAsync(envDownloader.canc);

            EnvFilesManager envFilemanager = new EnvFilesManager();
            envFilemanager.UpdateEnvFiles(envDownloader.HostList);

            ViewModelManager.UpdateViewModel(envDownloader, MainModel, OptionManager.GetOptions());
            EnvironmentsListView.Items.Refresh();
        }

        private void OnSelectedCategory(object sender, SelectionChangedEventArgs e)
        {
            Options options = OptionManager.GetOptions();
            options.LastSelectedCategory = MainModel.SelectedCategory;
            OptionManager.SaveOptions(options);
            UpdateVisible();
        }

        private void OnSelectedClick(object sender, SelectionChangedEventArgs e)
        {
            if (EnvironmentsListView.SelectedIndex > -1)
            {
                object selectedItem = EnvironmentsListView.SelectedItem;
                DeployEnvironmentModel selectedEnv = (DeployEnvironmentModel)selectedItem;
                FileContentTextBox.Text = selectedEnv.Content;
            }
        }

        public void CurrentHostDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (EnvironmentsListView.SelectedIndex < 0)
                return;

            DeployEnvironmentModel selectedEnv = new DeployEnvironmentModel();
            object selectedItem = EnvironmentsListView.SelectedItem;
            selectedEnv = (DeployEnvironmentModel)selectedItem;

            string contentToSave = GetHostContentToSave(selectedEnv);
            HostFileBuilder hostFileBuilder = new HostFileBuilder();
            if (!hostFileBuilder.SaveHost(contentToSave))
                return;

            ViewModelManager.UpdateCurrentHostProfileText(MainModel);

            Options options = OptionManager.GetOptions();
            options.LastFlushedEnvironment = selectedEnv.Name;
            OptionManager.SaveOptions(options);

            UpdateIcon(selectedEnv);

        }

        private string GetHostContentToSave(DeployEnvironmentModel selectedEnv)
        {
            string resp;

            if (MainModel.UseOverrides && !string.IsNullOrWhiteSpace(MainModel.OverrideEnv.Content))
                resp = CreateHostContentWithOverrides(selectedEnv);
            else
                resp = selectedEnv.Content;

            return resp;
        }

        private string CreateHostContentWithOverrides(DeployEnvironmentModel selectedEnv)
        {
            List<HostFileRow> overridesEntries = GetOverridesEntries();
            if (overridesEntries.Count == 0)
                return selectedEnv.Content;

            List<HostFileRow> envEntries = GetEnvironmentEntries(selectedEnv);
            return ApplySubstitution(envEntries, overridesEntries);

        }

        private string ApplySubstitution(List<HostFileRow> envEntries, List<HostFileRow> overridesEntries)
        {
            StringBuilder sb = new StringBuilder();

            int initialOverrides = overridesEntries.Count;
            foreach (HostFileRow sourceRow in envEntries)
            {
                if (!sourceRow.IsEntry)
                    sb.AppendLine(sourceRow.Content);
                else
                {
                    string substitute = GetEntryValue(sourceRow, overridesEntries);
                    sb.AppendLine(substitute);
                }
            }

            //Quando vengono sostituite nella procedura GetEntryValue
            //le entry vengono rimosse dalla lista delle overrides
            //Rimangono quindi solo le righe di overrides che non
            //sono state trovate (come domainName) nell'environment
            //Le scrivo in fondo al file.
            if (overridesEntries.Count > 0)
            {
                //foreach (HostFileRow row in overridesEntries)
                //{
                //    sb.Insert(0, $"{row.Content}{Environment.NewLine}");
                //    //sb.AppendLine(row.Content);
                //}
                for (int i = 0; i < overridesEntries.Count; i++)
                {
                    HostFileRow row = overridesEntries[i];
                    if (i==0)
                        sb.Insert(0, $"{row.Content}{Environment.NewLine}{Environment.NewLine}");
                    else
                        sb.Insert(0, $"{row.Content}{Environment.NewLine}");
                }
            }

            string header = GetSubstitutionHeader(initialOverrides, overridesEntries.Count);
            sb.Insert(0, $"{header}{Environment.NewLine}");


            return sb.ToString();
        }

        private string GetSubstitutionHeader(int initialOverrides, int addedCount)
        {
            StringBuilder sb = new StringBuilder();
            switch (addedCount)
            {
                case 0:
                    if (initialOverrides == 1)
                        sb.AppendLine("#Eseguita 1 sostituzione.");
                    else
                        sb.AppendLine($"#Eseguite {initialOverrides} sostituzioni.");
                    break;

                case 1:
                    if (initialOverrides == 2)
                        sb.AppendLine($"#Eseguita 1 sostituzione.");
                    else if (initialOverrides > 2)
                        sb.AppendLine($"#Eseguite {initialOverrides-1} sostituzioni.");

                    sb.AppendLine("#Eseguita 1 aggiunta.");

                    break;

                default:
                    if (initialOverrides - addedCount == 1)
                        sb.AppendLine($"#Eseguita 1 sostituzione.");
                    else if (initialOverrides - addedCount > 1)
                        sb.AppendLine($"#Eseguite {initialOverrides-addedCount} sostituzioni.");
                    sb.AppendLine($"#Eseguite {addedCount} aggiunte.");
                    break;
            }


            return sb.ToString();
        }

        private string GetEntryValue(HostFileRow sourceRow, List<HostFileRow> overridesEntries)
        {
            string domainName = sourceRow.DomainName;
            HostFileRow row = overridesEntries.FirstOrDefault(a => string.Equals(a.DomainName, domainName, StringComparison.OrdinalIgnoreCase));
            if (row is null)
                return sourceRow.Content;

            //E' necessario rimuoverle dalla lista, in modo che il chiamante
            //sappia se sono rimaste delle row (non presenti nell'environment)
            //da scrivere comunque nel file
            overridesEntries.Remove(row);

            StringBuilder sb = new StringBuilder();

            sb.AppendLine($"{OverridenPrefix}{sourceRow.Content}");
            sb.Append(row.Content);


            return sb.ToString();
        }

        private List<HostFileRow> GetEnvironmentEntries(DeployEnvironmentModel selectedEnv)
        {
            HostFileParser parser = new HostFileParser(selectedEnv.Content);
            return parser.Parse();
        }

        private List<HostFileRow> GetOverridesEntries()
        {
            HostFileParser parser = new HostFileParser(MainModel.OverrideEnv.Content);
            List<HostFileRow> rows = parser.Parse();

            return rows.Where(a => a.IsEntry).ToList();
        }

        public void UpdateCurrentHostProfileText()
        {
            ViewModelManager.UpdateCurrentHostProfileText(MainModel);
        }

        //private void LoadOptions()
        //{
        //    string fullFileName = GetOptionsFile();
        //    if (File.Exists(fullFileName))
        //        Options = SerializationHelper.Deserialize<Options>(fullFileName);
        //    else
        //        Options = new Options();

        //    ViewModelManager.UpdateOptions(MainModel,Options);

        //    UsedOverrideCheckBox.IsChecked = Options.UseOverridesSaved;
        //}

        //private void SaveOptions()
        //{
        //    SerializationHelper.Serialize(Options, GetOptionsFile());
        //}

        //private string GetOptionsFile()
        //{
        //    return Path.Combine(Environment.CurrentDirectory,"configs", "Options.xml");
        //}

        
        private void UpdateIcon(DeployEnvironmentModel selectedEnv)
        {
            foreach (var item in MainModel.Environments.Where(a => a.IsCurrent))
                item.IsCurrent = false;
            selectedEnv.IsCurrent = true;
        }

        public void UpdateVisible()
        {
            EnvironmentsListView.Items.Filter = UserFilter;
        }

        private void SaveOverridesTextClick(object sender, RoutedEventArgs e)
        {
            string overrideFileFullName = EnvironmentsContentDownloader.GetOverrideFileFullName();
            File.WriteAllText(overrideFileFullName, MainModel.OverrideEnv.Content);
        }

        private void CancelOverridesTextClick(object sender, RoutedEventArgs e)
        {
            ViewModelManager.UpdateOverrideEnv(MainModel, EnvironmentsContentDownloader.GetOverrideFileContent());
        }

        private void UseOverridesChecked(object sender, RoutedEventArgs e)
        {
            MainModel.UseOverrides = true;

            Options options = OptionManager.GetOptions();
            options.UseOverridesSaved = true;
            OptionManager.SaveOptions(options);
        }

        private void UseOverridesUnchecked(object sender, RoutedEventArgs e)
        {
            MainModel.UseOverrides = false;

            Options options = OptionManager.GetOptions();
            options.UseOverridesSaved = false;
            OptionManager.SaveOptions(options);
        }

    }

}




