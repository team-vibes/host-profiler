### FHGen
### User Version
### AREA: TEST-ASSISTENZA
### CREATION DATE: 2020-07-10 15:03:17Z




###############################
###### START HOST NAMES #######
###############################

#038-T920-PEC-EXENTRICA-LDAPEC-RED
10.150.20.38  arubapec.dbx.aruba

#039-912-GEOIPREPUTATION
10.150.12.39  geoiprep.aruba.it
10.150.12.39  pde.aruba.it

#042-TEST-FOTOALBUM-2016
95.110.151.42  blognew.aruba.it
95.110.151.42  fotoadmin.aruba.it
95.110.151.42  fotoalbumnew.aruba.it
95.110.151.42  fotostore.aruba.it

#042-TEST-FOTOALBUM-2016_49
95.110.151.49  redirect.blog.aruba.it
95.110.151.49  redirect2blog.aruba.it

#042-TEST-FOTOALBUM-2016_50
95.110.151.50  redirect2fotoalbum.aruba.it

#042-TEST-FOTOALBUM-2016_51
95.110.151.51  redirect2stat.aruba.it

#054-T920-WEBREG
10.150.20.54  crpanel.ictms.it
10.150.20.54  hss.aruba.it
10.150.20.54  whitelabel.private.ws.aruba.it
10.150.20.54  whitelabel.ws.aruba.it

#061-D912-WEBMAIL
10.150.12.61  webmail.aruba.it

#061-T920-MYSQL-BOMBOLO
10.150.20.61  normpec.dbx.aruba
10.150.20.61  registropec.dbx.aruba

#062-D912-VPOPMAIL
10.150.12.62  vpopmail.db.aruba

#067-T915-FATTURAPA-FE-VIP
10.150.15.67  fatturazioneelettronica.aruba.it

#070-D912-ATTIVAZIONE-DOMINI
10.150.12.70  api-legale-test.aruba.it
10.150.12.70  attivazionimail.aruba.it
10.150.12.70  attivazionimail2.aruba.it

#072-TEST-940-IPVSPROXY01
10.150.40.70  test-swpmysql.aruba.it

#075-T920-RDPGWDMZ
10.150.20.75  apipdn.aruba.it
10.150.20.75  mssqlinterno.staff.aruba.it

#080-T910-PECRIVWFACTALIS
10.150.10.80  areaclienti.actalis.it

#093-TEST-OPERATIONS
95.110.151.93  tt.staff.aruba.it

#097-T915-FEPA-TAI-FE
10.150.15.97  auth.fatturazioneelettronica.aruba.it
10.150.15.97  sdiws.fatturazioneelettronica.aruba.it
10.150.15.97  ws.fatturazioneelettronica.aruba.it

#10.50-CLA
10.150.10.50  cla.aruba.it
10.150.10.50  clabo.aruba.it
10.150.10.50  clastudio.aruba.it
10.150.10.50  clawizard.aruba.it
10.150.10.50  claws.aruba.it
10.150.10.50  dust.aruba.it
10.150.10.50  private.cla.aruba.it

#111-T918-FRONTENDCONS01
10.150.18.111  conservazione.docfly.it

#120-T915-FEPA-SILOG-API_VIP
10.150.15.120  elasticsearch-api.fatturazioneelettronica.aruba.it
10.150.15.120  order-api.fatturazioneelettronica.aruba.it
10.150.15.120  provisioner.fatturazioneelettronica.aruba.it

#130-T920-PEC-EXENTRICA-WEBPEC
10.150.20.130  gestionemail.pec.aruba.it

#137-TEST-GESTIONEFD
95.110.151.137  gestionefd.arubapec.it

#145-912-PHPMYADMIN-INTRANET
10.150.12.145  phpmy-intra.ad.aruba.it

#149-T920-INTRANET-AB
10.150.20.149  privateapi.arubabusiness.it
10.150.20.149  WsCbaPayCallback.arubabusiness.it

#156-STAG-PEC-EXENTRICA-WEBPECPRO
95.110.151.156  webmail.pec.it

#166-TEST-LOGSEARCH
95.110.151.166  rabbitlogmgr01.aruba.it
95.110.151.166  rabbitlogmgr02.aruba.it
95.110.151.166  rabbitlogmgr03.aruba.it
95.110.151.166  rabbitls.aruba.it

#167-TEST-WILDFLY
95.110.151.167  areaclienti.arubapec.it

#194_TEST_SUGARCRM
95.110.151.194  crm.aruba.it

#218-T920-WILDFLY-BALANCER
10.150.20.218  myaccount.aruba.it
10.150.20.218  myaccount.testops.aruba.it

#224-D910-OPENFIBER-DOCKER
10.150.10.224  api.radius.aruba.it

#226-T920-MONGO4.X-1
10.150.20.226  classidraregistro.db.aruba
10.150.20.226  gsa2.db.aruba
10.150.20.226  gsa2storico.db.aruba

#250-DEV-SQLDBARUBA7_2016
95.110.150.250  smartertrack.db.aruba

#ASKME_BE
10.150.20.117  supporto.aruba.it

#ASKME_FE
10.150.20.125  archiviorichieste.aruba.it

#BLOG
95.110.151.70  admin.blog.aruba.it
95.110.151.70  vts-blog.aruba.it
95.110.151.70  webservice.blog.aruba.it

#CERBERO-CBA-TEST
95.110.151.190  cba.aruba.it
95.110.151.190  wcfcba.aruba.it
95.110.151.190  WcfCBA_PasswordChanged_Notification.Aruba.it
95.110.151.190  WcfCBABancaSellaCallback.Aruba.it
95.110.151.190  WcfCBAIngenicoCallback.aruba.it
95.110.151.190  WcfCBAPayPalCallback.Aruba.it
95.110.151.190  wcfcbasec.aruba.it
95.110.151.190  WcfCerberoAlert.aruba.it
95.110.151.190  wcfcerberosecserver.aruba.it
95.110.151.190  wcfcerberoserver.aruba.it
95.110.151.190  websitecba.aruba.it
95.110.151.190  websitecerbero.aruba.it

#CLOUD
81.2.206.110  blacklistwebapi.forpsi.com
81.2.206.110  ws.forpsi.com
81.2.206.110  ws.forpsi.org

#CLOUD-0000
95.110.152.87  api.central.computing.cloud.it
95.110.152.87  services.central.cloud.it

#CLOUD-0001
95.110.152.88  api.private.computing.cloud.it

#CLOUD-0002
95.110.152.142  api.dc1.computing.cloud.it
95.110.152.142  services.dcs.cloud.it

#CLOUD-0004
95.110.152.98  api.dc2.computing.cloud.it

#CLOUD-0005
95.110.152.93  monitor01.computing.cloud.it

#CLOUD-0006
95.110.152.199  api.services.cloud.it

#CLOUD-LOG
95.110.152.84  ArubaCloudLog.db.aruba

#CONTABILITA
10.150.20.14  BillingManager.db.aruba
10.150.20.14  oceano.db.aruba
10.150.20.14  technorail.db.Oceano
10.150.20.14  technorailOceano.db.aruba

#CONTRATTI
95.110.151.239  documentiintranet
95.110.151.185  smtp.aruba.it
95.110.151.185  smtpcom.aruba.it
95.110.151.185  smtpcom.pec.aruba.it
95.110.151.239  wsshare.aruba.it

#FILEMANAGER
10.150.20.46  fm.aruba.it

#INTRANET
10.150.37.23  extranet.aruba.it
10.150.37.23  extranetnew.aruba.it
10.150.37.23  intranet.aruba.it
10.150.37.23  intranetdoc.aruba.it
10.150.37.23  intranetnew.ad.aruba.it
10.150.37.23  logmgrconfig.aruba.it
10.150.37.23  newoperatorarea.aruba.it
10.150.37.23  operatorarea.aruba.it
10.150.37.23  webfarm.intranet.aruba.it

#KENTICO
95.110.151.119  business.aruba.it
95.110.151.119  enterprise.aruba.it
95.110.151.119  guide.aruba.it
95.110.151.119  guide.arubabusiness.it
95.110.151.119  guide.hosting.aruba.it
95.110.151.119  guide.pec.it
95.110.151.119  guide.serverdedicati.aruba.it
95.110.151.119  hosting.aruba.it
95.110.151.119  kb.cloud.it
95.110.151.119  office365.aruba.it
95.110.151.119  promo.aruba.it
95.110.151.119  serverdedicati.aruba.it
95.110.151.119  staging.aruba.it
95.110.151.119  staging.hosting.aruba.it
95.110.151.119  staging.serverdedicati.aruba.it
95.110.151.119  www.actalis.it
95.110.151.119  www.aruba.it
95.110.151.119  www.arubacloud.com
95.110.151.119  www.cloud.it
95.110.151.119  www.datacenter.it
95.110.151.119  www.docfly.it
95.110.151.119  www.get.cloud
95.110.151.119  www.pec.it
95.110.151.119  www.worldof.cloud

#MOCK
95.110.151.234  wsnic.aruba.it

#MONGONSA01
10.150.10.17  ArubaNotificationMongo.db.aruba
10.150.10.17  ArubaNotificationMongo01.db.aruba
10.150.10.17  ArubaNotificationV2Mongo.db.aruba
10.150.10.17  ArubaNotificationV2Mongo01.db.aruba

#MONGONSA02
10.150.10.157  ArubaNotificationMongo02.db.aruba
10.150.10.157  ArubaNotificationV2Mongo02.db.aruba

#MONGONSA03
10.150.10.158  ArubaNotificationMongo03.db.aruba
10.150.10.158  ArubaNotificationV2Mongo03.db.aruba

#MSSQLPANEL
95.110.151.136  mssql.aruba.it
95.110.151.136  mssql2005.aruba.it
95.110.151.136  mssqlbackup.aruba.it
95.110.151.136  mssqlbackupnew.aruba.it
95.110.151.136  mssqlswitch.aruba.it

#MYSQL
95.110.151.237  admin.mysql.aruba.it
95.110.151.237  assistenza.db.aruba.it
95.110.151.237  cancellazione.db.aruba
95.110.151.218  cms_management.db.aruba
95.110.151.237  DNS.db.aruba
95.110.151.237  dns.dbx.aruba
95.110.151.237  ftp.dbx.aruba
95.110.151.237  ftp_analysis.dbx.aruba
95.110.150.240  imchat1.aruba.it
95.110.151.237  mysql.aruba.it
95.110.151.237  mysqldns.aruba.it 
95.110.151.237  pec_cmq.dbx.aruba
95.110.151.237  radius.aruba.it
95.110.151.158  RI_COMUNICAZIONE.dbx.aruba
95.110.151.237  servermysql.aruba.it
95.110.151.237  supportcenter.db.aruba
95.110.151.237  supportcenter_aruba.db.aruba

#MYSQL-GALERA
10.150.37.21  CustomerAreaLog.db.aruba
10.150.37.21  intranetmysql.db.aruba
10.150.37.21  NewOperatorAreaLog.db.aruba
10.150.37.21  onecartdb.db.aruba
10.150.37.21  onecartlogdb.db.aruba

#NUOVAARCHITETTURA
10.150.10.81  memcached1.aruba.it
95.110.151.60  SSLRobot

#NUOVA-ARCHITETTURA
10.150.37.22  account.aruba.it
10.150.37.22  adminarea.aruba.it
10.150.37.22  api.aruba.it
10.150.37.22  cart.aruba.it
10.150.37.22  cart.arubacloud.com
10.150.37.22  cart.cloud.it
10.150.37.22  customerarea.aruba.it
10.150.37.22  onecart.aruba.it
10.150.37.22  onecartcdn.aruba.it
10.150.37.22  preview_api.aruba.it
10.150.37.22  preview_cart.aruba.it
10.150.37.22  privapi-notification.aruba.it
10.150.37.22  rabbitip.aruba.it

#PDNS.AD.ARUBA.IT
10.150.12.11  pdns.ad.aruba.it

#PROVISIONING
95.110.151.104  cla_objects.db.keyprov
95.110.151.104  KEYPROV
95.110.151.104  provisioning.db.aruba
95.110.151.104  provisioning.ws.aruba.it

#REDIRECT1
10.150.20.51  redirect.aruba.it

#SITEBUILDER
95.110.151.178  admin.sitebuilder.aruba.it
95.110.151.178  adminsitebuilder.aruba.it

#SMARTERTRACK
10.150.10.11  kb.aruba.it
10.150.10.11  ticket.aruba.it

#SQL2008R2
95.110.151.199  ad.db.aruba
95.110.151.199  ApplicationInstallerDB.db.aruba
95.110.151.199  ArubaCatalog.db.aruba
95.110.151.199  ArubaCommSys.db.aruba
95.110.151.199  ArubaConfig.db.aruba
95.110.151.199  ArubaDelivery.db.aruba
95.110.151.199  ArubaDNS.db.aruba
95.110.151.199  arubalogmgr.db.aruba
95.110.151.199  ArubaManagement.db.aruba
95.110.151.199  ArubaMessageBroker.db.aruba
95.110.151.199  ArubaNotification.db.aruba
95.110.151.199  ArubaNotificationV2.db.aruba
95.110.151.199  ArubaRegistry.db.aruba
95.110.151.199  ArubaVoucher.db.aruba
95.110.151.199  ArubaWebApiLog.db.aruba
95.110.151.199  aspstate.db.aruba
95.110.151.199  aspState_pagamentiarubait.db.aruba
95.110.151.199  aspState_supersitearubait.db.aruba
95.110.151.199  bender.db.aruba
95.110.151.199  benderdata.db.aruba
95.110.151.199  BenderDBData.db.aruba
95.110.151.199  BenderDBFonte.db.aruba
95.110.151.199  BenderDBLog.db.aruba
95.110.151.199  benderfonte.db.aruba
95.110.151.199  benderlog.db.aruba
95.110.150.220  BuildServer.db.aruba
95.110.151.199  CallCenter.db.aruba
95.110.151.199  CambioCertificatoClient.db.aruba
95.110.151.199  CAuthoLog.ad.aruba
95.110.151.199  CAuthoLog.db.aruba
95.110.151.199  cla_objects.db.sql2008r2
95.110.151.199  Classidralog.db.aruba
95.110.151.199  CommEu.db.aruba
95.110.151.199  computing.cloud.fr.db.aruba
95.110.151.199  configurationmanagement.db.aruba
95.110.151.199  dnspanel.db.aruba
95.110.151.199  EasyFix.db.aruba
95.110.151.199  enewsletterpro_ENT.db.aruba
95.110.151.199  extranet.db.aruba
95.110.151.199  extranetnew.db.aruba
95.110.151.199  forpsi.customers-cloud.it.db.aruba
95.110.151.199  forpsi.shop-cloud.it.db.aruba
95.110.151.199  G6FTPBan.db.aruba
95.110.151.199  GTldPreferences.db.aruba
95.110.151.199  ict_intranet.db.aruba
95.110.151.199  Intranet_Administration.db.aruba
95.110.151.199  Intranet_Administrationnew.db.aruba
95.110.151.199  IntranetProcessi.db.aruba
95.110.151.199  IntranetUsersManager.db.aruba
95.110.151.199  IRSLive.db.aruba
95.110.151.199  LogVbsSchedulati.db.aruba
95.110.151.199  NicSystem.db.aruba
95.110.151.199  Operations.db.aruba
95.110.151.199  ParkedDomains.db.aruba
95.110.151.199  Photosi.db.aruba
95.110.151.199  provisioning_Backup.db.aruba
95.110.151.199  QueueService.db.aruba
95.110.151.199  ServiceManagement.db.aruba
95.110.151.199  SharedMailAdmin.db.aruba
95.110.151.199  SharedMailImport.db.aruba
95.110.151.199  SharedMailLog.db.aruba
95.110.151.199  SiteBuilder.db.aruba
95.110.151.199  Statistiche_VisiteSiti.db.aruba
95.110.151.199  Technorail_Lettura.db.aruba
95.110.151.199  tt.staff.aruba.it.db.aruba
95.110.151.199  WhiteListFTP.db.aruba
95.110.151.199  wsmiddleware.cloud.it.db.aruba

#SQL2014
95.110.151.175  cla_objects.db.sql1
95.110.151.175  Gestione_DNS.db.aruba
95.110.151.175  LegalRequests.db.aruba
95.110.151.175  OpenSrs.db.aruba
95.110.151.175  OpenSrsService.db.aruba
95.110.151.175  Procedure_Interne.db.aruba
95.110.151.175  RegistraOpenSRS.db.aruba
95.110.151.175  technorail.db.aruba

#SQLDBARUBA5
95.110.151.200  Aruba_PEC.db.aruba
95.110.151.200  ArubaLinking.db.aruba
95.110.151.200  ArubaPec.ad.aruba
95.110.151.200  ArubaPecMobileDevices.db.aruba
95.110.151.200  ArubaPecMobileLog.db.aruba
95.110.151.200  askme.db.aruba
95.110.151.200  ASPState_PhotoAdmin.db.aruba
95.110.151.200  ASPState_wwwpecit.db.aruba
95.110.151.200  Bannertechnorail.db.aruba
95.110.151.200  Blog.db.aruba
95.110.151.200  cla.db.aruba
95.110.151.200  cla_objects.db.sql5
95.110.151.200  clalog.db.aruba
95.110.151.200  ConfigParkDNN.db.aruba
95.110.151.200  ConfigParkDNN_STATS.db.aruba
95.110.151.200  DbNonFree.db.aruba
95.110.151.200  dbtablog.db.aruba
95.110.151.200  DocService2.db.aruba
95.110.151.200  Faq.db.aruba
95.110.151.200  FotoAlbum.db.aruba
95.110.151.200  FotoAlbumold.db.aruba
95.110.151.200  Gestionale.db.aruba
95.110.151.200  GestioneDNN.db.aruba
95.110.151.200  GestioneProcedure.db.aruba
95.110.151.200  GestioneStatistiche.db.aruba
95.110.151.200  Log_Radius_Adsl.db.aruba
95.110.151.200  OURSQL4
95.110.151.200  OURSQL4.db.aruba
95.110.151.200  pdn.db.aruba.it
95.110.151.200  PhotoSi.ad.aruba
95.110.151.200  QuadratureService.db.aruba
95.110.151.200  quadraturestatistiche.db.aruba
95.110.151.200  QuadratureWebs.db.aruba
95.110.151.200  REDBLOGFA.db.aruba
95.110.151.200  redirect.db.aruba
95.110.151.200  RedirectFotoAlbum.db.aruba
95.110.151.200  REDSTATS.db.aruba
95.110.151.200  SandBox.db.aruba
95.110.151.200  scambiobanner_banmanpro.db.aruba
95.110.151.200  scambiobanner_linkxpro.db.aruba
95.110.151.200  SetupServizi.db.aruba
95.110.151.200  SiteMapSpider.db.aruba
95.110.151.200  spostamentiwebs.db.aruba
95.110.151.200  SQLDBARUBA5
95.110.151.200  supporto_technorail.db.aruba
95.110.151.200  technorail_quadrature.db.aruba
95.110.151.200  technorail_tab_log.db.aruba
95.110.151.200  Uplinks.db.aruba
95.110.151.200  Uplinksfaq.db.aruba
95.110.151.200  virtuozzodb.db.aruba
95.110.151.200  Whitedb.db.aruba

#SQLDBARUBA-AO
95.110.151.85  ArubaConnectivityAddresses.db.aruba
95.110.151.85  ArubaPec.db.aruba
95.110.151.85  cla_objects.db.sql4
95.110.151.85  Classidra.db.aruba
95.110.151.85  FraudPS.db.aruba
95.110.151.85  gdpr.db.aruba
95.110.151.85  Gestione_MSSQLServer.db.aruba
95.110.151.85  gsa.db.aruba
95.110.151.85  PecClienti.db.aruba

#SWITE
94.177.219.14  stage.swite.com
94.177.219.14  swite.com
94.177.219.14  www.swite.com

#TEST-ONDA2
10.150.20.13  caronte.aruba.it

#UPDATEARUBA
95.110.151.99  update.aruba.it

#WEBDMZ
10.150.37.10  adsl.aruba.it
10.150.37.10  affiliazione.aruba.it
10.150.37.10  blog.aruba.it
10.150.37.10  cesta.arubacloud.es
10.150.37.10  cloudembedded.cloud.it
10.150.37.10  contratti.aruba.it
10.150.37.10  docs.computing.cloud.it
10.150.37.10  docs.services.cloud.it
10.150.37.10  embeddedcalculator.cloud.it
10.150.37.10  fatture.aruba.it
10.150.37.10  fotoalbum.aruba.it
10.150.37.10  gestioneaccessi.aruba.it
10.150.37.10  irs.aruba.it
10.150.37.10  keyposition.aruba.it
10.150.37.10  klient.arubacloud.pl
10.150.37.10  manageserverdedicati.aruba.it
10.150.37.10  megrendeles.arubacloud.hu
10.150.37.10  mobile.computing.cloud.it
10.150.37.10  pagamenti.aruba.it
10.150.37.10  panier.arubacloud.fr
10.150.37.10  pay.aruba.it
10.150.37.10  sms.aruba.it
10.150.37.10  smstarget.aruba.it
10.150.37.10  supersite.aruba.it
10.150.37.10  ugyfel.arubacloud.hu
10.150.37.10  WebSiteCBAPayPal.Aruba.it
10.150.37.10  zamowienie.arubacloud.pl

#WEBDMZHOST
10.150.37.11  admin.aruba.it
10.150.37.11  managehosting.aruba.it

#WEBDMZPEC
10.150.37.12  areaclienti.pec.it
10.150.37.12  manage.pec.it
10.150.37.12  rinnovifirmadigitale.pec.it
10.150.37.12  rinnovocdrl.pec.it
10.150.37.12  www.pecgratuita.it

#WEBSERVICE2012R2
10.150.37.20  apiaction.aruba.it
10.150.37.20  api-gsa.aruba.it
10.150.37.20  apimail.aruba.it
10.150.37.20  CBAPayCallBack.aruba.it
10.150.37.20  dataservices.ws.aruba.it
10.150.37.20  framework.ws.aruba.it
10.150.37.20  logmanager.aruba.it
10.150.37.20  logmanager.collector.aruba.it
10.150.37.20  logmanager.collector.cloud.it
10.150.37.20  mobile.pec.aruba.it
10.150.37.20  netwhois.aruba.it
10.150.37.20  netwhois.tol.aruba.it
10.150.37.20  netwhois2.aruba.it
10.150.37.20  netwhois3.aruba.it
10.150.37.20  netwhois4.aruba.it
10.150.37.20  netwhois5.aruba.it
10.150.37.20  netwhois6.aruba.it
10.150.37.20  notifications.pec.aruba.it
10.150.37.20  odino.aruba.it
10.150.37.20  openpublic.ws.aruba.it
10.150.37.20  paymentservice.aruba.it
10.150.37.20  photosi.ws.aruba.it
10.150.37.20  private.ws.aruba.it
10.150.37.20  privatesec.ws.aruba.it
10.150.37.20  public.ws.aruba.it
10.150.37.20  services.account.aruba.it
10.150.37.20  services.basekit.aruba.it
10.150.37.20  services.cart.aruba.it
10.150.37.20  services.configuration.aruba.it
10.150.37.20  services.domain.aruba.it
10.150.37.20  services.ws.aruba.it
10.150.37.20  swx.aruba.it
10.150.37.20  trustservicesapi.aruba.it
10.150.37.20  wcfwhois.aruba.it
10.150.37.20  webservice.aruba.it
10.150.37.20  webservice.arubacloud.fr
10.150.37.20  webservice2.aruba.it
10.150.37.20  webservice3.aruba.it
10.150.37.20  webservicecontabilita.aruba.it
10.150.31.20  webx394.aruba.it
10.150.37.20  ws1.aruba.it
10.150.37.20  ws2.aruba.it
10.150.37.20  ws3.aruba.it
10.150.37.20  ws4.aruba.it
10.150.37.20  ws5.aruba.it
10.150.37.20  ws6.aruba.it
10.150.37.20  wseinvoicing.aruba.it
10.150.37.20  wsinterni.aruba.it
10.150.37.20  wsinterni2.aruba.it
10.150.37.20  wsmiddleware.cloud.it

#WEBSERVICECONTABILITA
95.110.151.179  intranetfatture.aruba.it
95.110.151.179  intranetfatturesoci.aruba.it
95.110.151.179  ora.aruba.it
95.110.151.179  ora.staff.aruba.it
95.110.151.179  test-ora.aruba.it
95.110.151.179  test-ora.staff.aruba.it

#WEBSERVICEPEC
10.150.37.26  adminpecupload.pec.aruba.it
10.150.37.26  apiaction.kfd.aruba.it
10.150.37.26  apiaction.pec.aruba.it
10.150.37.26  exentrica.pec.aruba.it
10.150.37.26  PecDomini.pec.aruba.it
10.150.37.26  registropec.pec.aruba.it
10.150.37.26  services.pec.aruba.it
10.150.37.26  Upload.pec.aruba.it
10.150.37.26  Uploads.pec.aruba.it

#WIN_CLIENTI
31.11.32.254  ftp.nuovotrasferimento.eu
31.11.32.254  www.nuovotrasferimento.eu

#WSREDIS
10.150.20.28  wsredis.db.aruba


###############################
#### START AREA HOST NAMES ####
###############################


# SPID - test1
10.150.19.131 dbspid-mysql.aruba.it
10.150.19.151 dbspid-mongo.aruba.it
10.150.19.121 backendspid.aruba.it
10.150.19.122 monitorspid.aruba.it
10.150.19.170 loginspid-secure.aruba.it
10.150.19.171 loginspid.aruba.it
10.150.19.172 clientauthn-secure.aruba.it
10.150.19.173 registrazionespid.aruba.it
10.150.19.174 selfcarespid.aruba.it
10.150.19.177 migrazionespid.aruba.it
10.150.19.111 adminspid.aruba.it
10.150.19.175 spidtest.aruba.it
10.150.19.176 spspid.aruba.it
10.150.18.161 ftp.docfly.aruba.it

ACTALIS
10.150.20.56 test-tsafe.actalis.it

#MONITOR ASSISTENZA
10.150.20.104 nomad.aruba.it
10.150.20.104 jaeger.aruba.it
10.150.20.104 consul.aruba.it
10.150.20.104 grafana.aruba.it
10.150.20.104 icinga.aruba.it
10.150.20.104 prometheus.aruba.it

#OTHER
95.110.151.119  mediacdn.aruba.it

#CERBERO
95.110.151.193 CerberoCheckTicket.aruba.it
 95.110.151.193 WcfCBA_PasswordChanged_Notification.Aruba.it
 95.110.151.193 WcfCBABancaSellaCallback-test.Aruba.it
 95.110.151.190 WcfCerberoAlert.aruba.it
 95.110.151.190 wcfcerberosecserver.aruba.it
 95.110.151.190 wcfcerberoserver.aruba.it
 95.110.151.190 WebSiteAlertCerbero.aruba.it
 95.110.151.193 websitecba.aruba.it
 95.110.151.193 websitecerbero.aruba.it

#CLASSIDRA_DISTRO
10.150.20.187 gsaapi.aruba.it
10.150.20.187 classidraapi.aruba.it
10.150.20.187 apiactioncore.aruba.it
10.150.10.50 classidra.aruba.it
10.150.10.50 gsa.aruba.it
