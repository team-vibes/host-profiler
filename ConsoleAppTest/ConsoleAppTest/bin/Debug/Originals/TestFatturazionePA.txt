### FHGen
### User Version
### AREA: TEST_FATTPA
### CREATION DATE: 2020-07-10 15:03:18Z




###############################
###### START HOST NAMES #######
###############################

#066-T920-ESCLUSTER1
10.150.20.66  log1.elasticsearch.service.consul

#067-T915-FATTURAPA-FE-VIP
10.150.15.67  admin.fatturazioneelettronica.aruba.it
10.150.15.67  fatturazioneelettronica.aruba.it

#067-T920-ESCLUSTER2
10.150.20.67  log2.elasticsearch.service.consul

#068-T920-ESCLUSTER3
10.150.15.68  log3.elasticsearch.service.consul

#074-T915-FATTURAPA-PROV-VIP
10.150.15.74  invoiceapi.fatturazioneelettronica.aruba.it
10.150.15.74  masterdataapi.fatturazioneelettronica.aruba.it
10.150.15.74  sdiapi.fatturazioneelettronica.aruba.it
10.150.15.74  wsapi.fatturazioneelettronica.aruba.it

#087-T915-FEPA-TAI-FE-1_VIP_98
10.150.15.98  sdiws.fatturapa.aruba.it

#091-T915-MYSQL-1_VIP_90
10.150.15.90  db.fatturazioneelettronica.aruba.it

#097-T915-FEPA-TAI-FE
10.150.15.97  admin.fatturapa.aruba.it
10.150.15.97  auth.fatturazioneelettronica.aruba.it
10.150.15.97  sdiws.fatturazioneelettronica.aruba.it
10.150.15.97  testws.fatturazioneelettronica.aruba.it
10.150.15.97  ws.fatturapa.aruba.it
10.150.15.97  ws.fatturazioneelettronica.aruba.it

#111-T920-INFLUX
10.150.20.111  monitor.influxdb.service.consul

#114-T915-FEPA-TX-API-1_VIP
10.150.15.113  coreapitx.fatturazioneelettronica.aruba.it

#116-T915-FEPA-TX-BE-1
10.150.15.116  sbadmintx.fatturazioneelettronica.aruba.it

#120-T915-FEPA-SILOG-API_VIP
10.150.15.120  elasticsearch-api.fatturazioneelettronica.aruba.it
10.150.15.120  order-api.fatturazioneelettronica.aruba.it
10.150.15.120  provisioner.fatturazioneelettronica.aruba.it

#138-T915-FEPA-API-1_VIP
10.150.15.141  alert-receiver.fatturazioneelettronica.aruba.it
10.150.15.141  coreapi.fatturazioneelettronica.aruba.it
10.150.15.141  notice.fatturazioneelettronica.aruba.it

#140-T915-FEPA-BE-1
10.150.15.140  sbadminrx.fatturazioneelettronica.aruba.it

#166-TEST-LOGSEARCH
95.110.151.166  log1.logstash.service.consul
95.110.151.166  log2.logstash.service.consul
95.110.151.166  log3.logstash.service.consul
95.110.151.166  rabbitlogmgr01.aruba.it
95.110.151.166  rabbitlogmgr02.aruba.it
95.110.151.166  rabbitlogmgr03.aruba.it

#167-TEST-WILDFLY
95.110.151.167  areaclienti.arubapec.it

#KENTICO
95.110.151.119  business.aruba.it
95.110.151.119  enterprise.aruba.it
95.110.151.119  guide.aruba.it
95.110.151.119  guide.arubabusiness.it
95.110.151.119  guide.pec.it
95.110.151.119  guide.serverdedicati.aruba.it
95.110.151.119  kb.cloud.it
95.110.151.119  office365.aruba.it
95.110.151.119  promo.aruba.it
95.110.151.119  www.actalis.it
95.110.151.119  www.arubacloud.com
95.110.151.119  www.cloud.it
95.110.151.119  www.datacenter.it
95.110.151.119  www.get.cloud
95.110.151.119  www.worldof.cloud

#SQL2014
95.110.151.175  LegalRequests.db.aruba

#SQLDBARUBA-AO
95.110.151.85  ArubaConnectivityAddresses.db.aruba

#TEST-ONDA2
10.150.20.13  caronte.aruba.it

#WEBDMZPEC
10.150.31.12  areaclienti.pec.it

#WEBSERVICE2012R2
10.150.35.20  private.ws.aruba.it
10.150.35.20  privatesec.ws.aruba.it


###############################
#### START AREA HOST NAMES ####
###############################


10.150.39.28 vol.pec.local
10.150.15.77 elastic-search.fatturazione
10.150.15.79 elastic-search2.fatturazione
