### FHGen
### User Version
### AREA: TEST_SINGLESERVER
### CREATION DATE: 2020-07-10 15:03:17Z




###############################
###### START HOST NAMES #######
###############################

#042-TEST-FOTOALBUM-2016
95.110.151.42  blognew.aruba.it
95.110.151.42  fotoadmin.aruba.it
95.110.151.42  fotoalbumnew.aruba.it
95.110.151.42  fotostore.aruba.it

#042-TEST-FOTOALBUM-2016_49
95.110.151.49  redirect.blog.aruba.it
95.110.151.49  redirect2blog.aruba.it

#042-TEST-FOTOALBUM-2016_50
95.110.151.50  redirect2fotoalbum.aruba.it

#042-TEST-FOTOALBUM-2016_51
95.110.151.51  redirect2stat.aruba.it

#061-T920-MYSQL-BOMBOLO
10.150.20.61  normpec.dbx.aruba
10.150.20.61  registropec.dbx.aruba

#072-TEST-940-IPVSPROXY01
10.150.40.70  test-swpmysql.aruba.it

#093-TEST-OPERATIONS
95.110.151.93  tt.staff.aruba.it

#097-T915-FEPA-TAI-FE
10.150.15.97  auth.fatturazioneelettronica.aruba.it
10.150.15.97  sdiws.fatturazioneelettronica.aruba.it
10.150.15.97  ws.fatturazioneelettronica.aruba.it

#10.50-CLA
10.150.10.50  private.cla.aruba.it

#120-T915-FEPA-SILOG-API_VIP
10.150.15.120  order-api.fatturazioneelettronica.aruba.it

#166-TEST-LOGSEARCH
95.110.151.166  rabbitlogmgr01.aruba.it
95.110.151.166  rabbitlogmgr02.aruba.it
95.110.151.166  rabbitlogmgr03.aruba.it

#218-DEV-OPERATIONS
95.110.150.218  GestioneAmbienti.staff.aruba.it
95.110.150.218  operations.aruba.it

#ASKME_BE
10.150.20.117  supporto.aruba.it

#CLOUD-0002
95.110.152.142  services.dcs.cloud.it

#CONTRATTI
95.110.151.185  smtpcom.aruba.it
95.110.151.239  wsshare.aruba.it

#FILEMANAGER
10.150.20.46  fm.aruba.it

#KENTICO
95.110.151.119  business.aruba.it
95.110.151.119  enterprise.aruba.it
95.110.151.119  guide.aruba.it
95.110.151.119  guide.arubabusiness.it
95.110.151.119  guide.hosting.aruba.it
95.110.151.119  guide.pec.it
95.110.151.119  guide.serverdedicati.aruba.it
95.110.151.119  kb.cloud.it
95.110.151.119  office365.aruba.it
95.110.151.119  promo.aruba.it
95.110.151.119  www.actalis.it
95.110.151.119  www.arubacloud.com
95.110.151.119  www.cloud.it
95.110.151.119  www.datacenter.it
95.110.151.119  www.docfly.it
95.110.151.119  www.get.cloud
95.110.151.119  www.worldof.cloud

#MONGONSA01
10.150.10.17  ArubaNotificationMongo.db.aruba
10.150.10.17  ArubaNotificationMongo01.db.aruba
10.150.10.17  ArubaNotificationV2Mongo.db.aruba
10.150.10.17  ArubaNotificationV2Mongo01.db.aruba

#MONGONSA02
10.150.10.157  ArubaNotificationMongo02.db.aruba
10.150.10.157  ArubaNotificationV2Mongo02.db.aruba

#MONGONSA03
10.150.10.158  ArubaNotificationMongo03.db.aruba
10.150.10.158  ArubaNotificationV2Mongo03.db.aruba

#MSSQLPANEL
95.110.151.136  mssqlbackup.aruba.it
95.110.151.136  mssqlbackupnew.aruba.it
95.110.151.136  mssqlswitch.aruba.it

#MYSQL
95.110.151.237  radius.aruba.it

#PROVISIONING
95.110.151.104  cla_objects.db.keyprov

#REDIRECT1
10.150.20.51  redirect.aruba.it

#SITEBUILDER
95.110.151.178  adminsitebuilder.aruba.it

#SQL2008R2
95.110.151.199  ad.db.aruba
95.110.151.199  ArubaCatalog.db.aruba
95.110.151.199  ArubaNotificationV2.db.aruba
95.110.151.199  benderdata.db.aruba
95.110.151.199  benderfonte.db.aruba
95.110.151.199  benderlog.db.aruba
95.110.151.199  cla_objects.db.sql2008r2
95.110.151.199  Classidralog.db.aruba
95.110.151.199  LogVbsSchedulati.db.aruba
95.110.151.199  provisioning_Backup.db.aruba
95.110.151.199  tt.staff.aruba.it.db.aruba
95.110.151.199  wsmiddleware.cloud.it.db.aruba

#SQL2014
95.110.151.175  LegalRequests.db.aruba
95.110.151.175  technorail.db.aruba

#SQLDBARUBA5
95.110.151.200  askme.db.aruba
95.110.151.200  ASPState_PhotoAdmin.db.aruba
95.110.151.200  dbtablog.db.aruba
95.110.151.200  DocService2.db.aruba
95.110.151.200  FotoAlbum.db.aruba
95.110.151.200  REDBLOGFA.db.aruba
95.110.151.200  redirect.db.aruba
95.110.151.200  RedirectFotoAlbum.db.aruba
95.110.151.200  REDSTATS.db.aruba

#SQLDBARUBA-AO
95.110.151.85  ArubaConnectivityAddresses.db.aruba
95.110.151.85  Classidra.db.aruba
95.110.151.85  FraudPS.db.aruba
95.110.151.85  gdpr.db.aruba
95.110.151.85  Gestione_MSSQLServer.db.aruba

#SWITE
94.177.219.14  stage.swite.com
94.177.219.14  swite.com
94.177.219.14  www.swite.com

#WEBSERVICECONTABILITA
95.110.151.179  intranetfatture.aruba.it
95.110.151.179  intranetfatturesoci.aruba.it

#WIN_CLIENTI
31.11.32.254  ftp.nuovotrasferimento.eu
31.11.32.254  www.nuovotrasferimento.eu


###############################
#### START AREA HOST NAMES ####
###############################



