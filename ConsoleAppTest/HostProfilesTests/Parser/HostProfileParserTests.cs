﻿using System;
using System.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HostFileManager.Tests
{
    [TestClass]
    public class HostProfileParserTests
    {
        [TestMethod]
        public void HostProfileParser_Parse_WhenContainsEntry_Success()
        {
            string content = "### FHGen\r\n### User Version\r\n    10.150.10.92     cms_management.db.aruba #jhjg\r\n95.110.150.16  redirect2blog.aruba.it";
            HostFileParser parser = new HostFileParser(content);
            var result = parser.Parse();

            Assert.IsTrue(result.Count == 4);
            Assert.IsTrue(result[2].IsEntry);
            Assert.AreEqual("10.150.10.92", result[2].IpAddress);
            Assert.AreEqual("cms_management.db.aruba", result[2].DomainName);
        }
    }
}
